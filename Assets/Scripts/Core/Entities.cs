﻿using System;
using System.Collections.Generic;

namespace ROR.Core
{
    public class Entities
    {
        private Dictionary<long, IEntity> AllEntities = new Dictionary<long, IEntity>();
        private Random Random = new Random();
        public long GenerateNextEntityId()
        {
            long id;
            lock (AllEntities)
            {
                id = (long)Random.NextDouble();
                while (AllEntities.ContainsKey(id))
                {
                    continue;
                }
                
                AllEntities[id] = null;
            }

            return id;
        }
    }


    public interface IEntity
    {
        long EntityId { get; set; }
    }
}