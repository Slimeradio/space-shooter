﻿using System;
using UnityEngine;

namespace ROR.Core
{
    public class LivingEntity : MonoBehaviour
    {
        public EffectBar EffectBar;
        private Stats FinalStats;
        
        public float HP_NOW { get => FinalStats[Stats.HP_NOW]; private set => FinalStats[Stats.HP_NOW] = value; } 
        public float HP_MAX { get => FinalStats[Stats.HP_MAX]; private set => FinalStats[Stats.HP_MAX] = value; }

        protected virtual void Start()
        {
            Debug.Log("");
            FinalStats = new Stats();
            FinalStats[Stats.HP_MAX] = 100;
            FinalStats[Stats.HP_NOW] = FinalStats[Stats.HP_MAX];

            FinalStats[Stats.ATK_BASE] = 10;
            FinalStats[Stats.ATK_ADD] = 5;

            FinalStats[Stats.HP_REG] = 1;
            FinalStats[Stats.RANGE] = 600;
            FinalStats[Stats.ABSORB] = 2;
            FinalStats[Stats.RESIST] = 0.1f;
            FinalStats[Stats.MP_MAX] = 30f;
            FinalStats[Stats.MP_NOW] = FinalStats[Stats.MP_MAX];
            FinalStats[Stats.MP_NOW] = 1f;

            EffectBar = new EffectBar();
            EffectBar.Init(this);
        }
        
        void Update()
        {
            //EffectBar.Update(Time.deltaTime);
        }

        public event Action<LivingEntity> OnDeath; 
        
        public void Die()
        {
            OnDeath?.Invoke(this);
            Destroy(gameObject);
        }

        public void ChangeHP(float amount, object from)
        {
            if (amount > 0)
            {
                HP_NOW = Math.Min(HP_NOW + amount, HP_MAX);
            } 
            else if (amount < 0)
            {
                HP_NOW = Math.Max(HP_NOW + amount, 0);
                Debug.Log("HP_NOW:" + HP_NOW);
                if (HP_NOW <= 0)
                {
                    Die();
                }
            }
        }
    }
}