﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using UnityEngine;

namespace ROR.Core.Serialization
{
    public class Definition
    {
        public string Id;
    }

    public class BaseDefinition : Definition
    {
        public string Name;
        public string Icon;
        public string Description;
        public string Type;

        [OnDeserialized]
        public void PostSerialize()
        {
            Debug.Log("PostSerialize");
        }
    }
}