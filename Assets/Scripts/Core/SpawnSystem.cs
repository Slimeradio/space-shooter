﻿using UnityEngine;

namespace ROR.Core
{
    public class SpawnSystem : MonoBehaviour
    {
        public GameObject Point1;
        public GameObject Point2;
        public GameObject Prefab;


        private float TimePassed = 0;
        public float SpawnInterval = 3f;
        private int LastSpawn = 0;

        void Update()
        {
            TimePassed += Time.deltaTime;
            var iteration = (int) (TimePassed / SpawnInterval);
            if (LastSpawn != iteration)
            {
                var r = new System.Random();
                var dv = (Point1.transform.position - Point2.transform.position);
                dv.x = (float)r.NextDouble() * dv.x;
                dv.y = (float)r.NextDouble() * dv.y;
                dv.z = (float)r.NextDouble() * dv.z;
                var go = Instantiate(Prefab, dv + Point2.transform.position, Quaternion.identity);
                
                

                //CharacterController controller = FindObjectOfType<CharacterController>();
                //var enemy = go.GetComponent<EnemyController>();
                //enemy.SetNavDestination(controller.transform.position);
                
                LastSpawn = iteration;
            }
        }
    }
}