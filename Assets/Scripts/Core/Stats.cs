﻿namespace ROR.Core
{
    public class Stats
    {
        public const int HP_NOW = 0;
        public const int HP_MAX = 1;
        public const int HP_REG = 2;
    
        public const int MP_NOW = 3;
        public const int MP_MAX = 4;
        public const int MP_REG = 5;
    
        public const int ATK_BASE = 6;
        public const int ATK_ADD = 7;
        public const int ATK_REL = 8;
    
        public const int ATK_SPD = 9;
        public const int RELOAD_SPD = 10;
        public const int AMMO = 11;
        public const int PROJECTILES = 12;
        public const int RANGE = 13;

        public const int EFFECT_DURATION = 14;
        public const int EFFECT_POWER = 15;

        public const int HEALING_MLT = 16;
        public const int DOT_MLT = 17;
        public const int HOT_MLT = 18;
    
        public const int ABSORB = 19;
        public const int RESIST = 20;

    
        private float[] Data = new float[21];

        public float this[int index]
        {
            get => Data[index];
            set => Data[index] = value;
        }
    }
}
