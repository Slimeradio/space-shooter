﻿using System.Collections.Generic;
using ROR.Core;
using UnityEngine;

namespace Assets.Scripts.MyUnity
{
    public class Projectile : MonoBehaviour
    {
        private Vector3 Velocity;
        private float Lifetime;
        private LivingEntity Caster;
        public void Init(Vector3 velocity, LivingEntity caster)
        {
            Velocity = velocity;
            Caster = caster;
        }

        public void Update()
        {
            Lifetime += Time.deltaTime;
            var d = Velocity * Time.deltaTime;
            Debug.DrawLine(gameObject.transform.position, gameObject.transform.position+Velocity, Color.red);
            var p = gameObject.transform.position;
            var raycasts = new List<RaycastHit2D>();
            var start = new Vector2(p.x, p.y);
            var end = new Vector2(Velocity.x, Velocity.y);
            var filter = new ContactFilter2D();
            
            raycasts.Clear();
            if (Physics2D.Raycast(start, end, filter, raycasts, Velocity.magnitude*Time.deltaTime) > 0) //, out RaycastHit hit
            {
                foreach (var x in raycasts)
                {
                    var livingEntity = x.collider.gameObject.GetComponentInParent<LivingEntity>();
                    if (livingEntity != null)
                    {
                        continue;    
                    }
                    Debug.Log("Hit! " + x.collider.gameObject);
                    var projectile = x.collider.gameObject.GetComponentInChildren<Projectile>();
                    if (projectile != null)
                    {
                        //if (projectile.Caster != Caster)
                        {
                            Destroy(gameObject);
                            Destroy(projectile.gameObject);
                            return;
                        }
                    }
                    
                    //var livingEntity = x.collider.gameObject.GetComponentInParent<LivingEntity>();
                    //if (livingEntity != null)
                    //{
                    //    if (livingEntity != Caster)
                    //    {
                    //        livingEntity.ChangeHP(-10, this);
                    //        Destroy(gameObject);
                    //        return;
                    //    }
                    //}
                }
                
            }
            
            gameObject.transform.position += d;

            if (Lifetime > 8f)
            {
                Destroy(gameObject);
            }
        }
    }
}