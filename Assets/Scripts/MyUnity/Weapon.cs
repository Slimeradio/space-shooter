﻿using System;
using ROR;
using ROR.Core;
using ROR.Lib;
using UnityEngine;

namespace Assets.Scripts.MyUnity
{
    public class Weapon : MonoBehaviour
    {
        public Transform MuzzlePoint;
        public GameObject Bullet;
        public bool CanTarget = false;
        private Timer Timer = new Timer(0, 0.25f);
        private Vector3 m_targetPoint = new Vector3();
        private Ship m_ship;

        void OnStart()
        {
            if (Bullet == null)
            {
                throw new Exception("Bullet not set");
            }
            
            m_ship = GetComponentInParent<Ship>();
        }
        
        void Update()
        {
            Fire (CanTarget ? m_targetPoint : MuzzlePoint.transform.right);
        }

        public void Target(Vector3 targetPoint)
        {
            if (!CanTarget) return;
            m_targetPoint = targetPoint;
        }
        
        public void Fire(Vector3 pointing)
        {
            if (Timer.Tick(Time.deltaTime))
            {
                var bullet = Instantiate(Bullet, MuzzlePoint.position, Quaternion.identity);
                var projectile = bullet.GetComponent<Projectile>();
                var velocity = (pointing - MuzzlePoint.position).normalized * 3;
                projectile.Init(velocity, m_ship);
            }
        }
    }
}