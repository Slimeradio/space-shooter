using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(GameObject))]
class MapGenerator : Editor
{
    public GameObject Editor;
    public int X = 3;
    public int Z = 3;
    public float Y = 1f;
    
    public float SizeX = 1;
    public float SizeZ = 1;
    
    public float StepY = 0.2f;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        
        Editor = (GameObject) EditorGUILayout.ObjectField(Editor, typeof(GameObject), true);
        
        X = EditorGUILayout.IntField(X);
        Z = EditorGUILayout.IntField(Z);
        Y = EditorGUILayout.FloatField(Y);
        SizeX = EditorGUILayout.FloatField(SizeX);
        SizeZ = EditorGUILayout.FloatField(SizeZ);
        StepY = EditorGUILayout.FloatField(StepY);
        
        if (GUILayout.Button("Click"))
        {
            Generate();
        }
    }

    public void Generate()
    {
        
        var where = Selection.activeGameObject;
        Debug.LogWarning(Selection.activeGameObject);

        var l = where.transform.childCount;
        
        //for (int i = 0; i < l; i++)
        //{
        //    var child = where.transform.GetChild(0);
        //    var o = child.gameObject;
        //    GameObject.DestroyImmediate(o);
        //}

        //EditorSceneManager.OpenScene(where.scene.path);
        var r = new System.Random();
        
        
        Debug.Log($"Generate {X} {Y} {Z}");
        var w = 1;
        var h = 1;
        var d = 1;

        var offset = new Vector3(X * w / 2 * SizeX, Y * h / 2, Z * d / 2 * SizeZ);
        
        for (int x = 0; x < X; x++)
        {
            for (int z = 0; z < Z; z++)
            {
                var pos = new Vector3();
                pos.x = x * SizeX;
                pos.y = (float)(r.NextDouble()*Y)*h;
                pos.z = z * SizeZ;

                pos.y = pos.y - pos.y % StepY;
                
                var go= Instantiate(Editor, where.transform.position + pos - offset, where.transform.rotation);
                go.transform.SetParent(where.transform);
                go.transform.localScale = new Vector3(1, 1, 1);
            }
        }
        //EditorApplication.SaveScene(where.scene.path);
        
    }

}